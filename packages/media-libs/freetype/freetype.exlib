# Copyright 2007 Alexander Færøy <ahf@exherbo.org>
# Copyright 2010 Bo Ørsted Andresen <zlin@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'freetype-2.3.7.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

export_exlib_phases src_prepare src_compile src_install

SUMMARY="A portable font engine"
HOMEPAGE="https://www.${PN}.org"
DOWNLOADS="https://download.savannah.gnu.org/releases/${PN}/${PNV}.tar.xz
    utils? ( https://download.savannah.gnu.org/releases/${PN}/ft2demos-$(ever range 1-3).tar.xz )"

REMOTE_IDS="freecode:${PN} sourceforge:${PN}"

UPSTREAM_RELEASE_NOTES="https://sourceforge.net/projects/${PN}/files/${PN}2/${PV}/NEWS/view"

LICENCES="|| ( FTL GPL-2 ) MIT ZLIB public-domain"
SLOT="2"
# harfbuzz option added to allow breaking a cycle between freetype and harfbuzz
MYOPTIONS="
    harfbuzz [[ description = [ improve auto-hinting of OpenType fonts ] ]]
    utils
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.24]
    build+run:
        app-arch/bzip2
        media-libs/libpng:=
        sys-libs/zlib
        harfbuzz? ( x11-libs/harfbuzz[>=1.3.0] )
        utils? (
            x11-libs/libX11
            x11-libs/libXau
            x11-libs/libXdmcp
        )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
    --enable-freetype-config
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( harfbuzz )

freetype_src_prepare() {
    default

    local headerdir
    headerdir=include/${PN}/config

    enable_option() {
        edo sed -i -e "/#define ${1}/a #define ${1}" ${headerdir}/ftoption.h
    }
    disable_option() {
        edo sed -i -e "/#define ${1}/ { s:^:/*:; s:$:*/: }" ${headerdir}/ftoption.h
    }

    enable_option FT_CONFIG_OPTION_INCREMENTAL

    # See http://freetype.org/patents.html about potential patent infringement issues
    enable_option FT_CONFIG_OPTION_SUBPIXEL_RENDERING

    enable_option "TT_CONFIG_OPTION_SUBPIXEL_HINTING  2"

    enable_option PCF_CONFIG_OPTION_LONG_FAMILY_NAMES

    if option utils; then
        edo sed -i -e "s:\.\.\/freetype2$:../freetype-${PV}:" "${WORKBASE}"/ft2demos-$(ever range 1-3)/Makefile
    fi
}

freetype_src_compile() {
    default

    if option utils; then
        edo pushd "${WORKBASE}"/ft2demos-$(ever range 1-3)/
        emake FT2DEMOS=1 TOP_DIR_2="${PWD}" -f "${WORKBASE}"/ft2demos-$(ever range 1-3)/Makefile
        edo popd
    fi
    # make refdoc would build API documentation, but needs docwriter
    # (https://pypi.org/project/docwriter/), which has annoying exact version
    # deps.
}

freetype_src_install() {
    default

    if option utils; then
        edo rm "${WORKBASE}"/ft2demos-$(ever range 1-3)/bin/README
        for ft2demo in ../ft2demos-$(ever range 1-3)/bin/*; do
            edo libtool --mode=install $(type -P install) -m 755 "$ft2demo" "${IMAGE}"/usr/$(exhost --target)/bin/
        done
    fi
}

