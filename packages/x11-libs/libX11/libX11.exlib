# Copyright 2007 Alexander Færøy <ahf@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require xorg

export_exlib_phases src_compile

SUMMARY="Core X11 protocol client library"

LICENCES="libX11"
SLOT="0"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        x11-libs/xtrans
        x11-proto/xorgproto
        x11-utils/util-macros[>=1.19.0-r1] [[
            note = [ Introduced a patch for XORG_PROG_RAWCPP to avoid the use of unprefixed cpp ]
        ]]
        doc? ( app-text/xmlto[>=0.0.22] )
    build+run:
        x11-libs/libxcb[>=1.11.1]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    CC_FOR_BUILD="$(exhost --target)-cc"
    --without-fop
    # Necessary for crosscompiling
    --disable-malloc0returnsnull
    # X.org wants xlocale.. Don't remove it.
    --enable-xlocale
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'doc specs' )
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( 'doc xmlto' )

libX11_src_compile() {
    # makekeys (built by libX11 itself) is run during the build process. This
    # obviously fails when cross-compiling for an arch which can't be run on
    # the host. Since makekeys isn't installed we can just build it with the
    # host compiler.
    if ! exhost --is-native -q; then
        emake -C src/util \
            BUILD_CC="$(exhost --build)-cc" \
            CC="$(exhost --build)-cc" \
            clean all
    fi

    default
}

