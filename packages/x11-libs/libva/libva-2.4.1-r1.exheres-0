# Copyright 2009, 2010, 2014 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=intel tag=${PV} ]
require meson

SUMMARY="Userspace Video Acceleration (VA) core interface"
DESCRIPTION="
The main motivation for VAAPI (Video Acceleration API) is to enable hardware
accelerated video decode/encode at various entry-points (VLD, IDCT, Motion
Compensation etc.) for the prevailing coding standards today (MPEG-2, MPEG-4
ASP/H.263, MPEG-4 AVC/H.264, and VC-1/VMW3). Extending XvMC was considered, but
due to its original design for MPEG-2 MotionComp only, it made more sense to
design an interface from scratch that can fully expose the video decode
capabilities in today's GPUs.
"
HOMEPAGE+=" https://01.org/linuxmedia/vaapi"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    X
    doc
    wayland
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? ( app-doc/doxygen )
    build+run:
        x11-dri/libdrm[>=2.4]
        X? (
            x11-dri/mesa[X?]
            x11-libs/libX11
            x11-libs/libXext
            x11-libs/libXfixes
        )
        wayland? ( sys-libs/wayland[>=1.11.0] )
    suggestion:
        x11-drivers/intel-driver [[ description = [ libva HW video decode support for Intel integrated graphics ] ]]
        x11-libs/vdpau-video [[ description = [ libva HW video decode support for VDPAU platforms. e.g. NVIDIA ] ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddisable_drm=false
    -Denable_va_messaging=true
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'doc enable_docs'
    'X with_x11 yes no'
    'X with_glx yes no'
    'wayland with_wayland yes no'
)

