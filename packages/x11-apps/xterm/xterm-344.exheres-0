# Copyright 2008 Alexander Færøy <ahf@exherbo.org>
# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require freedesktop-desktop gtk-icon-cache

SUMMARY="X Terminal Emulator"
HOMEPAGE="https://invisible-island.net/${PN}"
DOWNLOADS="https://invisible-mirror.net/archives/${PN}/${PNV}.tgz"

UPSTREAM_CHANGELOG="${HOMEPAGE}/${PN}.log.html#${PN}_${PV}"

LICENCES="X11"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="truetype"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        x11-apps/luit
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libXaw
        x11-libs/libXinerama
        x11-libs/libxkbfile
        x11-libs/libXmu
        x11-libs/libXpm
        x11-libs/libXrender
        x11-libs/libXt
        truetype? (
            media-libs/fontconfig
            media-libs/freetype:2
            x11-libs/libXft
        )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --hates=docdir
    --enable-256-color
    --enable-broken-st
    --enable-dabbrev
    --enable-desktop
    --enable-direct-color
    --enable-i18n
    --enable-load-vt-fonts
    --enable-luit
    --enable-mini-luit
    --enable-openpty
    --enable-readline-mouse
    --enable-regex
    --enable-screen-dumps
    --enable-tcap-query
    --enable-toolbar
    --enable-wide-attrs
    --enable-wide-chars
    --disable-builtin-xpms
    --disable-setgid
    --disable-setuid
    --disable-sixel-graphics
    --with-app-defaults=/usr/share/X11/app-defaults
    --with-icon-theme=hicolor
    --with-icondir=/usr/share/icons
    --with-xinerama
    --without-Xaw3d
    --without-Xaw3dxft
    --without-XawPlus
    --without-man2html
    --without-pcre
    --without-pcre2
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'truetype freetype' )

src_prepare() {
    edo sed \
        -e "/GenericName=/s:#::" \
        -e "/Icon=/s:_48x48::" \
        -i xterm.desktop

    default
}

src_install() {
    default

    insinto /usr/share/applications
    doins ${PN}.desktop
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

